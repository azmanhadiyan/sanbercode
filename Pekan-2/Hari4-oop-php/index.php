<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");
echo "Name :  $sheep->name <br>";
echo "Legs :  $sheep->legs <br>";
echo "Cold Blooded :  $sheep->cold_blooded";

echo "<br><br>";

$sungokong  = new Ape("shaun");
echo $sungokong->yell();

echo "<br><br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
?>
